import Vue from 'vue'
import IndexPage from '@/components/IndexPage'

describe('IndexPage.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(IndexPage)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
})
